use crate::vector::Vec3;

pub struct Light {
	pub position: Vec3<f64>,
	pub intensity: f64,
}