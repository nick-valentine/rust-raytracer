use crate::vector::Vec3;
use crate::material::Material;

pub trait Intersector {
	fn intersect(&self, origin: Vec3<f64>, dir: Vec3<f64>, t0: &mut f64) -> bool;
}

#[derive(Debug, Clone, Copy)]
pub struct Sphere {
	pub center: Vec3<f64>,
	pub radius: f64,
	pub material: Material
}

impl Sphere {
	pub fn new(center: Vec3<f64>, radius: f64, material: Material) -> Sphere {
		Sphere{
			center: center,
			radius: radius,
			material: material,
		}
	}
}

impl Intersector for Sphere {
	fn intersect(&self, origin: Vec3<f64>, dir: Vec3<f64>, t0: &mut f64) -> bool {
		let l = self.center.sub(&origin);
		let tca = l.dot(&dir);
		let d2 = l.dot(&l) - (tca * tca);

		if d2 > (self.radius * self.radius) {
			return false;
		}
		let thc = ((self.radius * self.radius) - d2).sqrt();
		*t0 = tca - thc;
		let t1 = tca + thc;
		if *t0 < 0.0 {
			*t0 = t1
		}
		if *t0 < 0.0 {
			return false;
		}
		return true;
	}
}