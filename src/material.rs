use crate::vector::{Vec3,Vec4};

#[derive(Debug, Clone, Copy)]
pub struct Material {
	pub albedo: Vec4<f64>,
	pub diffuse_color: Vec3<f64>,
	pub specular_exponent: f64,
	pub refractive_index: f64,
}

impl Material {
	pub fn new(albedo: Vec4<f64>, diffuse: Vec3<f64>, specular: f64, refractive: f64) -> Material {
		Material{
			albedo: albedo,
			diffuse_color: diffuse,
			specular_exponent: specular,
			refractive_index: refractive,
		}
	}

	pub fn zero() -> Material {
		Material{
			albedo: Vec4{
				w: 0.0, x: 0.0,
				y: 0.0, z: 0.0
			},
			diffuse_color: Vec3{
				x: 0.0, y: 0.0, z: 0.0
			},
			specular_exponent: 0.0, 
			refractive_index: 0.0,
		}
	}
}