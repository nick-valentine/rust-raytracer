

#[derive(Debug, Clone, Copy)]
pub struct Vec2<T> {
	pub x: T,
	pub y: T,
}

#[derive(Debug, Clone, Copy)]
pub struct Vec3<T> {
	pub x: T,
	pub y: T,
	pub z: T,
}

#[derive(Debug, Clone, Copy)]
pub struct Vec4<T> {
	pub w: T,
	pub x: T,
	pub y: T,
	pub z: T,
}

impl<T> Vec3<T> 
	where T: Copy + 
		std::ops::Add<Output = T> +
		std::ops::Sub<Output = T> +
		std::ops::Mul<Output = T> + 
		std::ops::AddAssign
{
	pub fn new(x: T, y: T, z: T) -> Vec3<T> {
		Vec3{x: x, y: y, z: z}
	}

	pub fn sub(&self, other: &Vec3<T>) -> Vec3<T> {
		Vec3{
			x: self.x - other.x,
			y: self.y - other.y,
			z: self.z - other.z,
		}
	}
	
	pub fn add(&self, other: &Vec3<T>) -> Vec3<T> {
		Vec3{
			x: self.x + other.x,
			y: self.y + other.y,
			z: self.z + other.z,
		}
	}

	pub fn dot(&self, other: &Vec3<T>) -> T {
		let mut out = self.x * other.x;
		out += self.y * other.y;
		out += self.z * other.z;
		out
	}

	pub fn scale(&self, by: &T) -> Vec3<T> {
		Vec3{
			x: self.x * *by,
			y: self.y * *by,
			z: self.z * *by,
		}
	}
}

impl Vec3<f64> {
	pub fn zero() -> Vec3<f64> {
		Vec3{x: 0.0, y: 0.0, z: 0.0}
	}

	pub  fn negate(&self) -> Vec3<f64> {
		Vec3{
			x: -1.0 * self.x,
			y: -1.0 * self.y,
			z: -1.0 * self.z,
		}
	}
	
	pub fn normalize(&self) -> Vec3<f64> {
		let divisor = self.length();
		Vec3{
			x: self.x / divisor,
			y: self.y / divisor,
			z: self.z / divisor,
		}
	}

	pub fn length(&self) -> f64 {
		((self.x * self.x) + (self.y * self.y) + (self.z * self.z)).sqrt()
	}
}