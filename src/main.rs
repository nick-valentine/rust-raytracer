#[macro_use]
extern crate bmp;

mod vector;
mod shapes;
mod material;
mod lights;
mod render;

use futures::executor::block_on;

use crate::vector::*;
use crate::shapes::*;
use crate::material::*;
use crate::lights::*;
use crate::render::render;
use bmp::{Image, Pixel};

fn main() {

	let ivory = Material::new(
		Vec4{w: 0.6, x: 0.3, y: 0.1, z: 0.0},
		Vec3{x: 0.4, y: 0.4, z: 0.3},
		50.0, 1.0,
	);
	let glass = Material::new(
		Vec4{w: 0.1, x: 0.1, y: 0.05, z: 0.9},
		Vec3{x: 0.6, y: 0.7, z: 0.8},
		12.0, 1.0,
	);
	let red_rubber = Material::new(
		Vec4{w: 0.9, x: 0.1, y: 0.0, z: 0.0},
		Vec3{x: 0.3, y: 0.1, z: 0.1},
		10.0, 1.0,
	);
	let mirror = Material::new(
		Vec4{w: 0.9, x: 0.1, y: 0.8, z: 0.0},
		Vec3{x: 0.1, y: 0.1, z: 0.1},
		1425.0, 1.0,
	);

	let mut spheres = Vec::new();
	spheres.push(Sphere::new(
		Vec3{x: -3.0, y: 0.0, z: -16.0},
		2.0, ivory,
	));
	spheres.push(Sphere::new(
		Vec3{x: -1.0, y: -1.5, z: -12.0},
		2.0, glass,
	));
	spheres.push(Sphere::new(
		Vec3{x: 1.5, y: -0.5, z: -18.0},
		3.0, red_rubber,
	));
	spheres.push(Sphere::new(
		Vec3{x: 7.0, y: 5.0, z: -18.0},
		4.0, mirror,
	));

	let mut lights = Vec::new();
	lights.push(Light{
		position: Vec3{x: -20.0, y: 20.0, z: 20.0},
		intensity: 1.5,
	});
	lights.push(Light{
		position: Vec3{x: 30.0, y: 50.0, z: -25.0},
		intensity: 1.8,
	});
	lights.push(Light{
		position: Vec3{x: 30.0, y: 20.0, z: -30.0},
		intensity: 1.7,
	});

	let size = Vec2{x: 1024, y: 728};

	let mut framebuffer = Vec::new();
	framebuffer.resize(size.x * size.y, Vec3::<f64>::zero());

	block_on(render(&mut framebuffer, size, 45.0, &spheres, &lights));
	write_image(&framebuffer, size);

}

fn write_image(framebuffer: &[Vec3<f64>], size: Vec2<usize>) {
	let mut img = Image::new(size.x as u32, size.y as u32);

	let brightness_correction = 0.4;
	let gamma_correction = 0.8;

	for (x, y) in img.coordinates() {
		let mut pixel = framebuffer[(x + (y * size.x as u32)) as usize];
		
		pixel = pixel.scale(&brightness_correction);

		pixel.x = pixel.x.powf(1.0 / gamma_correction);
		pixel.y = pixel.y.powf(1.0 / gamma_correction);
		pixel.z = pixel.z.powf(1.0 / gamma_correction);

		let largest = pixel.x.max(pixel.y.max(pixel.z));
		if largest > 1.0 {
			pixel = pixel.scale(&(1.0 / largest));
		}

		img.set_pixel(x, y, px!(pixel.x * 255.0, pixel.y * 255.0, pixel.z * 255.0))
	}

	let _ = img.save("out.bmp");
}