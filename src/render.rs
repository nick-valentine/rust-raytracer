use futures::future::join_all;

use crate::vector::*;
use crate::shapes::*;
use crate::material::*;
use crate::lights::*;

fn reflect(i: &Vec3<f64>, n: &Vec3<f64>) -> Vec3<f64> {
	let scalar = 2.0 * i.dot(n);
	let scaled = n.scale(&scalar);
	i.sub(&scaled)
}

fn refract(i: &Vec3<f64>, n: &Vec3<f64>, refractive_index: f64) -> Vec3<f64> {
	let mut cosi = -1.0_f64 * (-1.0_f64).max((1.0_f64).min(i.dot(n)));
	let mut etai = 1.0;
	let mut etat = refractive_index;

	let mut normal = n.clone();
	if cosi < 0.0 {
		cosi *= -1.0;
		etai = refractive_index;
		etat = 1.0;
		normal = normal.negate();
	}
	let eta = etai / etat;
	let k = 1.0 - eta  * eta * (1.0 - cosi * cosi);
	if k < 0.0 {
		return Vec3::<f64>::zero()
	}
	let scalar = eta * cosi * k.sqrt();
	normal = normal.scale(&scalar);
	let _i = i.scale(&eta);
	normal.add(i)
}

fn scene_intersect(orig: &Vec3<f64>, dir: &Vec3<f64>, spheres: &[Sphere], hit: &mut Vec3<f64>, normal: &mut Vec3<f64>, material: &mut Material) -> bool {
	let mut sphere_dist = f64::MAX;
	for i in spheres.iter() {
		let mut dist_i = f64::MAX;
		if i.intersect(*orig, *dir, &mut dist_i) {
			if dist_i < sphere_dist {
				sphere_dist = dist_i;
				let scaled = dir.scale(&dist_i);
				*hit = orig.add(&scaled);
				let n = hit.sub(&i.center);
				*normal = n.normalize();
				*material = i.material;
			}
		}
	}

	let mut checkerboard_dist = f64::MAX;
	if dir.y.abs() > 1e-3 {
		let d = -(orig.y + 4.0) / dir.y;
		let scaled = dir.scale(&d);
		let pt = orig.add(&scaled);
		if d > 0.0 && pt.x.abs() < 10.0 && pt.z > -30.0 && d < sphere_dist {
			checkerboard_dist = d;
			*hit = pt;
			*normal = Vec3::new(0.0, 1.0, 0.0);
			if ((0.5 * hit.x + 1000.0) as i64 + (0.5 * hit.z) as i64) & 1  == 0 {
				*material = Material::new(
					Vec4{
						w: 0.2, x: 0.2,
						y: 0.9, z: 0.0
					},
					Vec3{
						x: 0.2, y: 0.2, z: 0.2
					},
					0.1, 1.0
				)
			} else {
				*material = Material::new(
					Vec4{
						w: 0.1, x: 0.3,
						y: 0.1, z: 0.0
					},
					Vec3{
						x: 0.1, y: 0.3, z: 0.1
					},
					0.1, 1.0
				)
			}
		}
	}
	(sphere_dist.min(checkerboard_dist)) < 60.0
}

fn cast_ray(origin: &Vec3<f64>, dir: &Vec3<f64>, spheres: &[Sphere], lights: &[Light], depth: i64) -> Vec3<f64> {
	let offset = 1e-3;

	let mut point = Vec3::<f64>::zero();
	let mut normal = Vec3::<f64>::zero();
	let mut material = Material::zero();

	if depth > 4 || !scene_intersect(origin, dir, spheres, &mut point, &mut normal, &mut material) {
		return Vec3::new(0.2, 0.2, 0.3);
	}

	let reflect_dir = reflect(dir, &normal).normalize();

	let mut reflect_origin = Vec3::<f64>::zero();
	if reflect_dir.dot(&normal) < 0.0 {
		let scaled = normal.scale(&offset);
		reflect_origin = point.sub(&scaled);
	} else {
		let scaled = normal.scale(&offset);
		reflect_origin = point.add(&scaled);
	}

	let reflect_color = cast_ray(&reflect_origin, &reflect_dir, spheres, lights, depth + 1);


	let refract_dir = refract(dir, &normal, material.refractive_index).normalize();
	
	let mut refract_origin = Vec3::<f64>::zero();
	if refract_dir.dot(&normal) < 0.0 {
		let scaled = normal.scale(&offset);
		refract_origin = point.sub(&scaled);
	} else {
		let scaled = normal.scale(&offset);
		refract_origin = point.add(&scaled);
	}

	let refract_color = cast_ray(&refract_origin, &refract_dir, spheres, lights, depth + 1);


	let mut diffuse_light_intensity = 0.0;
	let mut specular_light_intensity = 0.0;

	for i in lights.iter() {
		let light_dir = i.position.sub(&point);
		let light_distance = light_dir.length();
		let light_dir = light_dir.normalize();

		let mut shadow_orig = Vec3::<f64>::zero();
		if light_dir.dot(&normal) < 0.0 {
			let scaled = normal.scale(&offset);
			shadow_orig = point.sub(&scaled);
		} else {
			let scaled = normal.scale(&offset);
			shadow_orig = point.add(&scaled);
		}

		let mut shadow_pt = Vec3::<f64>::zero();
		let mut shadow_n = Vec3::<f64>::zero();
		let mut tmp_mat = Material::zero();

		if scene_intersect(&shadow_orig, &light_dir, spheres, &mut shadow_pt, &mut shadow_n, &mut tmp_mat) {
			let diff = shadow_pt.sub(&shadow_orig);
			let len = diff.length();
			if len < light_distance {
				continue
			}
		}

		diffuse_light_intensity += i.intensity * light_dir.dot(&normal).max(0.0);
		let neg_light_dir = light_dir.negate();
		let refl = reflect(&neg_light_dir, &normal).negate();
		specular_light_intensity += refl.dot(&dir).max(0.0).powf(material.specular_exponent) * i.intensity;
	}

	let diffuse = material.diffuse_color.scale(&diffuse_light_intensity);
	let mut specular = Vec3::new(1.0, 1.0, 1.0);
	specular = specular.scale(&(specular_light_intensity * material.albedo.w));
	specular = specular.add(&Vec3::new(material.albedo.x, material.albedo.x, material.albedo.x));
	let refl = reflect_color.scale(&material.albedo.y);
	let refr = refract_color.scale(&material.albedo.z);

	diffuse.add(&specular).add(&refl).add(&refr)
}

async fn render_rows(row: usize, count: usize, size: Vec2<usize>, fov: f64, spheres: &[Sphere], lights: &[Light]) -> Vec<Vec3<f64>> {
	let mut row_buffer = Vec::new();
	row_buffer.resize(size.x * count, Vec3::<f64>::zero());
	let zero = Vec3::<f64>::zero();
	for i in row..row+count {
		for j in 0..size.x {
			let x = (2.0 * (j as f64 + 0.5) / size.x as f64 - 1.0) * (fov / 2.0).tan() * size.x as f64 / size.y as f64;
			let y = -(2.0 * (i as f64 + 0.5) / size.y as f64 - 1.0) * (fov / 2.0).tan();
			let dir = Vec3::new(x, y, -1.0).normalize();
			row_buffer[j + ((i-row) * size.x)] = cast_ray(&zero, &dir, spheres, lights, 0);
		}
	}
	row_buffer
}

pub async fn render(framebuffer: &mut[Vec3<f64>], size: Vec2<usize>, fov: f64, spheres: &[Sphere], lights: &[Light]) {
	let divisor = size.y / 16;
	let mut futures = Vec::new();
	futures.reserve(size.y / divisor);
	for i in 0..size.y/divisor {
		futures.push(render_rows(i*divisor, divisor, size, fov, spheres, lights));
	}
	let lines = join_all(futures).await;
	for i in 0..lines.len() {
		for j in 0..lines[i].len() {
			framebuffer[j + (i * size.x * divisor)] = lines[i][j];
		}
	}
}